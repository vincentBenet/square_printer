TODO:
    * Electronique
        - Régler la régulation de température sur la buse

    * Configuration
        - Régler la valeur de pullup sur la thermistance du plateau

    * Mécanique
        - Recevoir les plaques en découpe laser de Xometrie
        - Faire le pliage des plaques de Hotend

    * Documentation
        - Créer une documentation Sphinx
        - Mettre les plans PDF de la Hotend sur Sphinx

    * Code
        - Passer le code de calibration sous python3
