#include <AccelStepper.h>
#include <MultiStepper.h>

#define MZ4MICRO 25
#define MX2EN 51
#define MZ4EN 50
#define MX2STEP 6
#define MZ4STEP 7
#define MX2DIR 47
#define MZ4DIR 48
#define MX2MICRO 49
#define MZ3MICRO 60
#define MY2EN 61
#define MZ3EN 62
#define MY2STEP 10
#define MZ3STEP 11
#define MY2DIR 36
#define MZ3DIR 64
#define MY2MICRO 63
#define MZ2MICRO 38
#define MX1EN 65
#define MZ2EN 66
#define MX1STEP 8
#define MZ2STEP 9
#define MX1DIR 40
#define MZ2DIR 52
#define MX1MICRO 53
#define MZ1MICRO 24
#define MY1EN 2
#define MZ1EN 3
#define MY1STEP 12
#define MZ1STEP 13
#define MY1DIR 26
#define MZ1DIR 59
#define MY1MICRO 58
#define MF2EN 23
#define TQHBUSE 34
#define TQVBUSE 32
#define TQVRAD 30
#define THBUSE 68
#define MF1STEP 5
#define MF2STEP 4
#define MF1DIR 22
#define MF2DIR 42
#define MF1MICRO 44
#define MF2MICRO 45
#define MF1EN 46
#define EX1 54
#define EX2 55
#define EY2 56
#define EY1 57


AccelStepper MZ1 = AccelStepper(1, MZ1STEP, MZ1DIR);
AccelStepper MZ2 = AccelStepper(1, MZ2STEP, MZ2DIR);
AccelStepper MZ3 = AccelStepper(1, MZ3STEP, MZ3DIR);
AccelStepper MZ4 = AccelStepper(1, MZ4STEP, MZ4DIR);
AccelStepper MX1 = AccelStepper(1, MX1STEP, MX1DIR);
AccelStepper MX2 = AccelStepper(1, MX2STEP, MX2DIR);
AccelStepper MY1 = AccelStepper(1, MY1STEP, MY1DIR);
AccelStepper MY2 = AccelStepper(1, MY2STEP, MY2DIR);
AccelStepper MF1 = AccelStepper(1, MF1STEP, MF1DIR);
AccelStepper MF2 = AccelStepper(1, MF2STEP, MF2DIR);

MultiStepper steppers_x;
MultiStepper steppers_y;

int way_x = 1;
int way_y = 1;
int steps = 2;
long positions_x[2];
long positions_y[2];

void setup() {
    
  MX1.setMaxSpeed(500000);
  MX1.setAcceleration(500000);
  MX2.setMaxSpeed(500000);
  MX2.setAcceleration(500000);
  MY1.setMaxSpeed(500000);
  MY1.setAcceleration(500000);
  MY2.setMaxSpeed(500000);
  MY2.setAcceleration(500000);
  
  steppers_x.addStepper(MX1);
  steppers_x.addStepper(MX2);
  steppers_y.addStepper(MY1);
  steppers_y.addStepper(MY2);

  pinMode(EX1, INPUT);
  pinMode(EX2, INPUT);
  pinMode(EY1, INPUT);
  pinMode(EY2, INPUT);

  positions_x[0] = 0;
  positions_x[1] = 0;
  positions_y[0] = 0;
  positions_y[1] = 0;

  pinMode(MX1MICRO, OUTPUT);
  pinMode(MX2MICRO, OUTPUT);
  pinMode(MY1MICRO, OUTPUT);
  pinMode(MY2MICRO, OUTPUT);
  
  digitalWrite(MX1MICRO, LOW);
  digitalWrite(MX2MICRO, LOW);
  digitalWrite(MY1MICRO, LOW);
  digitalWrite(MY2MICRO, LOW);
  
}

void move_axis_x(int nb_steps){
  positions_x[0] = positions_x[0] + nb_steps;
  positions_x[1] = positions_x[1] - nb_steps;
  steppers_x.moveTo(positions_x);
  steppers_x.runSpeedToPosition();
}

void move_axis_y(int nb_steps){
  positions_y[0] = positions_y[0] + nb_steps;
  positions_y[1] = positions_y[1] - nb_steps;
  steppers_y.moveTo(positions_y);
  steppers_y.runSpeedToPosition();
}


bool endstop_x(){return (!digitalRead(EX1) or !digitalRead(EX2));}
bool endstop_y(){return (!digitalRead(EY1) or !digitalRead(EY2));}

void home_axis_x(){
    move_axis_x(way_x * steps);
    if (endstop_x()){
      while (endstop_x()){move_axis_x(-way_x * steps);};
      digitalWrite(MX1MICRO, HIGH);
      digitalWrite(MX2MICRO, HIGH);
      while (not(endstop_x())){move_axis_x(way_x);};
      digitalWrite(MX1MICRO, LOW);
      digitalWrite(MX2MICRO, LOW);
      while (endstop_x()){move_axis_x(-way_x * steps);};
      way_x = -1 * way_x;
    };
}

void home_axis_y(){
    move_axis_y(way_y * steps);
    if (endstop_y()){
      while (endstop_y()){move_axis_y(-way_y * steps);};
      digitalWrite(MY1MICRO, HIGH);
      digitalWrite(MY2MICRO, HIGH);
      while (not(endstop_y())){move_axis_y(way_y);};
      digitalWrite(MY1MICRO, LOW);
      digitalWrite(MY2MICRO, LOW);
      while (endstop_y()){move_axis_y(-way_y * steps);};
      way_y = -1 * way_y;
    };
}

void loop() {
  home_axis_x();
  home_axis_y();
}
