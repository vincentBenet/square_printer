import matplotlib.pyplot as plt
import random
import numpy
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon


def intersect(x1, y1, x2, y2, x3, y3, x4, y4):
    return (
        ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)), 
        ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    )

class Printer:
    def __init__(self,
        longeur=200,  # x
        largeur=300,  # y
        hauteur=400,  # z
        ratio_xy=10,  # ratio between one rotation of motor axis and offset of ballscrew in meter
        micro=32,  # Microstepping when activated (used for axis calibration)
        steps=24  # Number of normal steps in one revolution
    ):
        # Attribution des arguments à l'objet
        self.longeur = longeur
        self.largeur = largeur
        self.hauteur = hauteur
        self.ratio_xy = ratio_xy
        self.micro = micro
        self.steps = steps
        
        self.pos_x_z1 = 0
        self.pos_y_z1 = 0
        self.pos_x_z2 = longeur
        self.pos_y_z2 = 0
        self.pos_x_z3 = longeur
        self.pos_y_z3 = largeur
        self.pos_x_z4 = 0
        self.pos_y_z4 = largeur
        self.noise_measure = 1 # mm de bruit de mesure
        self.z_mesure = 30  # mm de distance avec la butée z max
        
        self._pos = None
        self._axis = [
            random.random() * longeur,
            random.random() * longeur,
            random.random() * largeur,
            random.random() * largeur,
        ]
        self._plateau = [
            random.random() * hauteur - self.z_mesure,
            random.random() * hauteur - self.z_mesure,
            random.random() * hauteur - self.z_mesure,
            random.random() * hauteur - self.z_mesure,
        ]
        
        self.fig, (self.ax_x, self.ax_y, self.ax_z) = plt.subplots(3)
        self.fig.canvas.mpl_connect('key_press_event', self.on_press)
        self.ax_x.axis("equal")
        self.ax_y.axis("equal")
        self.ax_z.axis("equal")
        self.plot3D()
        plt.show()
    
    @property
    def pos(self):
        return intersect(self._axis[0], 0, self._axis[1], self.largeur, 0, self._axis[2], self.longeur, self._axis[3])
    
    @property
    def x(self):
        return self.pos[0]
        
    @property
    def y(self):
        return self.pos[1]
    
    @property
    def x1(self):
        return self._axis[0]
    
    @x1.setter
    def x1(self, value):
        self._axis[0] = value

    @property
    def x2(self):
        return self._axis[1]
    
    @x2.setter
    def x2(self, value):
        self._axis[1] = value

    @property
    def y1(self):
        return self._axis[2]
    
    @y1.setter
    def y1(self, value):
        self._axis[2] = value

    @property
    def y2(self):
        return self._axis[3]
    
    @y2.setter
    def y2(self, value):
        self._axis[3] = value

    @property
    def z1(self):
        return self._plateau[0]
    
    @z1.setter
    def z1(self, value):
        self._plateau[0] = value
    
    @property
    def z2(self):
        return self._plateau[1]
    
    @z2.setter
    def z2(self, value):
        self._plateau[1] = value
    
    @property
    def z3(self):
        return self._plateau[2]
    
    @z3.setter
    def z3(self, value):
        self._plateau[2] = value
    
    @property
    def z4(self):
        return self._plateau[3]
    
    @z4.setter
    def z4(self, value):
        self._plateau[3] = value
    
    def z(self, x=None, y=None):
        if x is None:
            x = self.x
        if y is None:
            y = self.y
        
        x0 = self.longeur / 2
        y0 = self.largeur / 2
        z0 = (self.z1 + self.z2 + self.z3 + self.z4) / 4 
    
 
        poss = [
            [self.pos_x_z1, self.pos_y_z1, self.z1],
            [self.pos_x_z2, self.pos_y_z2, self.z2],
            [self.pos_x_z3, self.pos_y_z3, self.z3],
            [self.pos_x_z4, self.pos_y_z4, self.z4]
        ]
 
        point = Point(x, y)
        n = len(poss)
        for i in range(n):
            polygon = Polygon([(x0, y0), (poss[i][0], poss[i][1]), (poss[(i+1)%n][0], poss[(i+1)%n][1]), (x0, y0)])
            if polygon.buffer(1e-14).contains(point):
                break
        else:
            raise Exception(f"Position ({x, y}) not on the plateau: [{poss}]")
        
        x1, y1, z1 = [x0, y0, z0]
        x2, y2, z2 = poss[i]
        x3, y3, z3 = poss[(i+1)%n]
        
        # x1, y1, z1 = [self.pos_x_z1, self.pos_y_z1, self.z1]
        # x2, y2, z2 = [self.pos_x_z2, self.pos_y_z2, self.z2]
        # x3, y3, z3 = [self.pos_x_z3, self.pos_y_z3, self.z3]
        
        
        
        a1 = x2 - x1
        b1 = y2 - y1
        c1 = z2 - z1
        a2 = x3 - x1
        b2 = y3 - y1
        c2 = z3 - z1
        a = b1 * c2 - b2 * c1
        b = a2 * c1 - a1 * c2
        c = a1 * b2 - b1 * a2
        d = -a * x1 - b * y1 - c * z1
        return -(a * x + b * x + d) / c

    def on_press(self, event):
        key_pressed = event.key
        self.keyhandler(key_pressed)
        self.plot3D()
    
    def move(self, x, way, micro):
        return x * (2 * way - 1) / [1, self.micro][micro]
    
    def move_x(self, way, x, micro=None):
        if micro is None:
            micro = [False] * 2
        self.x1 = max(0, min(self.longeur, self.x1 + self.move(x, way, micro[0])))
        self.x2 = max(0, min(self.longeur, self.x2 + self.move(x, way, micro[1])))
    
    def move_y(self, way, x, micro=None):
        if micro is None:
            micro = [False] * 2
        self.y1 = max(0, min(self.largeur, self.y1 + self.move(x, way, micro[0])))
        self.y2 = max(0, min(self.largeur, self.y2 + self.move(x, way, micro[1])))
        
    def move_z(self, way, x, micro=None):
        if micro is None:
            micro = [False] * 4
        self.z1 = min(self.hauteur, self.z1 + self.move(x, way, micro[0]))
        self.z2 = min(self.hauteur, self.z2 + self.move(x, way, micro[1]))
        self.z3 = min(self.hauteur, self.z3 + self.move(x, way, micro[2]))
        self.z4 = min(self.hauteur, self.z4 + self.move(x, way, micro[3]))
    
    def measure(self, x=None, y=None):
        while True:
            measure_i = self.z(x, y) + (1 - 2 * random.random()) * self.noise_measure
            if measure_i > self.hauteur - self.z_mesure:
                break
            self.move_z(True, self.ratio_xy / self.micro / self.steps)
        return measure_i
    
    def keyhandler(self, key_pressed):
        move = self.ratio_xy
        {
            '6': lambda: self.move_x(True, move),
            'ctrl+6': lambda: self.move_x(True, move, micro=[True, False]),
            'alt+6': lambda: self.move_x(True, move, micro=[False, True]),
            '4': lambda: self.move_x(False, move),
            'ctrl+4': lambda: self.move_x(False, move, micro=[True, False]),
            'alt+4': lambda: self.move_x(False, move, micro=[False, True]),
            '8': lambda: self.move_y(True, move),
            'ctrl+8': lambda: self.move_y(True, move, micro=[True, False]),
            'alt+8': lambda: self.move_y(True, move, micro=[False, True]),
            
            '2': lambda: self.move_y(False, move),
            'ctrl+2': lambda: self.move_y(False, move, micro=[True, False]),
            'alt+2': lambda: self.move_y(False, move, micro=[False, True]),
            
            '5': lambda: self.move_z(True, move),
            'alt+7': lambda: self.move_z(True, move, micro=[True, True, True, False]),
            'alt+9': lambda: self.move_z(True, move, micro=[True, True, False, True]),
            'alt+1': lambda: self.move_z(True, move, micro=[False, True, True, True]),
            'alt+3': lambda: self.move_z(True, move, micro=[True, False, True, True]),
            
            '0': lambda: self.move_z(False, move),
            'ctrl+7': lambda: self.move_z(False, move, micro=[True, True, True, False]),
            'ctrl+9': lambda: self.move_z(False, move, micro=[True, True, False, True]),
            'ctrl+1': lambda: self.move_z(False, move, micro=[False, True, True, True]),
            'ctrl+3': lambda: self.move_z(False, move, micro=[True, False, True, True]),
            
            'ctrl+5': lambda: self.measure(),
            
        }.get(key_pressed, lambda: None)()
        
        print("#" * 100)
        print(f"{self.x = }")
        print(f"{self.x1 = }")
        print(f"{self.x2 = }")
        print(f"{self.y = }")
        print(f"{self.y1 = }")
        print(f"{self.y2 = }")
        print(f"{self.z() = }")
        print(f"{self.z1 = }")
        print(f"{self.z2 = }")
        print(f"{self.z3 = }")
        print(f"{self.z4 = }")
        print(f"{self.z4 = }")


    def plot3D(self):
        from matplotlib import cm
        from matplotlib.ticker import LinearLocator

        fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

        X = numpy.arange(0, self.longeur, 10)
        Y = numpy.arange(0, self.largeur, 10)
        Z = []
        
        for i, x in enumerate(X):
            Z.append([])
            for y in Y:
                Z[i].append(self.z(x, y))

        Z = numpy.array(Z).T
        X, Y = numpy.meshgrid(X, Y)
        
        surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=0, antialiased=False)
        
        xss = [self.pos_x_z1, self.pos_x_z2, self.pos_x_z3, self.pos_x_z4]
        yss = [self.pos_y_z1, self.pos_y_z2, self.pos_y_z3, self.pos_y_z4]
        zss = [self.z1, self.z2, self.z3, self.z4]
        
        x0 = self.longeur / 2
        y0 = self.largeur / 2
        z0 = numpy.array(zss).mean()
        
        ax.scatter(
            xss, 
            yss, 
            zss, 
            marker="o"
        )
        
        for i in range(4):
            ax.plot(
                [xss[i], x0, xss[(i+1)%4], xss[i]], 
                [yss[i], y0, yss[(i+1)%4], yss[i]], 
                [zss[i], z0, zss[(i+1)%4], zss[i]]
            )
        
        ax.set_zlim(min(zss), max(zss))
        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter('{x:.02f}')
        fig.colorbar(surf, shrink=0.5, aspect=5)

        plt.show()


    def plot(self):
        self.ax_x.cla()
        self.ax_y.cla()
        self.ax_z.cla()

        x, y = self.pos

        self.ax_x.axhline(
            y=self.hauteur,
            color = "gray",
            linestyle="--"
        )
        self.ax_x.axhline(
            y=0,
            color = "gray",
            linestyle="--"
        )
        self.ax_x.axvline(
            x=0,
            color = "gray",
            linestyle="--"
        )
        self.ax_x.axvline(
            x=self.largeur,
            color = "gray",
            linestyle="--"
        )
        self.ax_x.plot(
            [self.y1, self.y2],
            [self.hauteur, self.hauteur],
            color = "red",
        )
        self.ax_x.plot(
            [0, self.largeur],
            [self.z1, self.z2],
            color = "pink",
        )
        self.ax_x.scatter(
            [y], [self.hauteur],
            color = "green"
        )
        
        self.ax_y.axhline(
            y=self.hauteur,
            color = "gray",
            linestyle="--"
        )
        self.ax_y.axhline(
            y=0,
            color = "gray",
            linestyle="--"
        )
        self.ax_y.axvline(
            x=0,
            color = "gray",
            linestyle="--"
        )
        self.ax_y.axvline(
            x=self.longeur,
            color = "gray",
            linestyle="--"
        )
        self.ax_y.plot(
            [self.x1, self.x2],
            [self.hauteur, self.hauteur],
            color = "blue",
            linestyle="-"
        )
        self.ax_y.plot(
            [0, self.longeur],
            [self.z3, self.z4],
            color = "yellow",
        )
        self.ax_y.scatter(
            [x], [self.hauteur],
            color = "green"
        )
        
        self.ax_z.axhline(
            y=0,
            color = "gray",
            linestyle="--"
        )
        self.ax_z.axhline(
            y=self.largeur,
            color = "gray",
            linestyle="--"
        )
        self.ax_z.axvline(
            x=0,
            color = "gray",
            linestyle="--"
        )
        self.ax_z.axvline(
            x=self.longeur,
            color = "gray",
            linestyle="--"
        )
        self.ax_z.plot(
            [self.x1, self.x2],
            [0, self.largeur],
            color = "blue",
            linestyle="-"
        )
        self.ax_z.plot(
            [0, self.longeur],
            [self.y1, self.y2],
            color = "red",
            linestyle="-"
        )
        self.ax_z.scatter(
            [x], [y],
            color = "green"
        )
        
        self.fig.canvas.draw()

if __name__ == "__main__":
    # Initialisation des positions
    simu = Printer()
    