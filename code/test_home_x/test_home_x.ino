#include <AccelStepper.h>
#include <MultiStepper.h>

#define MZ4MICRO 25
#define MX2EN 51
#define MZ4EN 50
#define MX2STEP 6
#define MZ4STEP 7
#define MX2DIR 47
#define MZ4DIR 48
#define MX2MICRO 49
#define MZ3MICRO 60
#define MY2EN 61
#define MZ3EN 62
#define MY2STEP 10
#define MZ3STEP 11
#define MY2DIR 36
#define MZ3DIR 64
#define MY2MICRO 63
#define MZ2MICRO 38
#define MX1EN 65
#define MZ2EN 66
#define MX1STEP 8
#define MZ2STEP 9
#define MX1DIR 40
#define MZ2DIR 52
#define MX1MICRO 53
#define MZ1MICRO 24
#define MY1EN 2
#define MZ1EN 3
#define MY1STEP 12
#define MZ1STEP 13
#define MY1DIR 26
#define MZ1DIR 59
#define MY1MICRO 58
#define MF2EN 23
#define TQHBUSE 34
#define TQVBUSE 32
#define TQVRAD 30
#define THBUSE 68
#define MF1STEP 5
#define MF2STEP 4
#define MF1DIR 22
#define MF2DIR 42
#define MF1MICRO 44
#define MF2MICRO 45
#define MF1EN 46
#define EX1 54
#define EX2 55
#define EY2 56
#define EY1 57


AccelStepper MZ1 = AccelStepper(1, MZ1STEP, MZ1DIR);
AccelStepper MZ2 = AccelStepper(1, MZ2STEP, MZ2DIR);
AccelStepper MZ3 = AccelStepper(1, MZ3STEP, MZ3DIR);
AccelStepper MZ4 = AccelStepper(1, MZ4STEP, MZ4DIR);
AccelStepper MX1 = AccelStepper(1, MX1STEP, MX1DIR);
AccelStepper MX2 = AccelStepper(1, MX2STEP, MX2DIR);
AccelStepper MY1 = AccelStepper(1, MY1STEP, MY1DIR);
AccelStepper MY2 = AccelStepper(1, MY2STEP, MY2DIR);
AccelStepper MF1 = AccelStepper(1, MF1STEP, MF1DIR);
AccelStepper MF2 = AccelStepper(1, MF2STEP, MF2DIR);

MultiStepper steppers;

int way = 1;
int steps = 1;
long positions[2];
long sides[2];

void setup() {
    
  MX1.setMaxSpeed(500000);
  MX1.setAcceleration(500000);
  MX2.setMaxSpeed(500000);
  MX2.setAcceleration(500000);
  
  steppers.addStepper(MX1);
  steppers.addStepper(MX2);

  pinMode(EX1, INPUT);
  pinMode(EX2, INPUT);

  positions[0] = 0;
  positions[1] = 0;

  pinMode(MX1EN, OUTPUT);
  pinMode(MX2EN, OUTPUT);
  
  pinMode(MX1MICRO, OUTPUT);
  pinMode(MX2MICRO, OUTPUT);

  digitalWrite(MX1EN, LOW);
  digitalWrite(MX2EN, LOW);
  
  digitalWrite(MX1MICRO, HIGH);
  digitalWrite(MX2MICRO, HIGH);
}

void move_axis(int nb_steps){
  positions[0] = positions[0] + nb_steps;
  positions[1] = positions[1] - nb_steps;
  steppers.moveTo(positions);
  steppers.runSpeedToPosition();
}

void loop() {
  while (digitalRead(EX2)){move_axis(way * steps);}
  sides[0] = positions[0];
  while (digitalRead(EX1)){move_axis(- way * steps);}
  sides[1] = positions[1];
  move_axis(positions[1] - positions[0] / 2);
}
