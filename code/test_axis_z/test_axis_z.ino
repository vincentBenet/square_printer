#include <AccelStepper.h>
#include <MultiStepper.h>

// Pole 4
#define MZ4MICRO 25
#define MX2EN 51
#define MZ4EN 50
#define MX2STEP 6
#define MZ4STEP 7
#define MX2DIR 47
#define MZ4DIR 48
#define MX2MICRO 49

// Pole 3
#define MZ3MICRO 60
#define MY2EN 61
#define MZ3EN 62
#define MY2STEP 10
#define MZ3STEP 11
#define MY2DIR 36
#define MZ3DIR 64
#define MY2MICRO 63

// Pole 2
#define MZ2MICRO 38
#define MX1EN 65
#define MZ2EN 66
#define MX1STEP 8
#define MZ2STEP 9
#define MX1DIR 40
#define MZ2DIR 52
#define MX1MICRO 53

// Pole 1
#define MZ1MICRO 24
#define MY1EN 2
#define MZ1EN 3
#define MY1STEP 12
#define MZ1STEP 13
#define MY1DIR 26
#define MZ1DIR 59
#define MY1MICRO 58

// Hotend
#define MF2EN 23
#define TQHBUSE 34
#define TQVBUSE 32
#define TQVRAD 30
#define THBUSE 68
#define MF1STEP 5
#define MF2STEP 4
#define MF1DIR 22
#define MF2DIR 42
#define MF1MICRO 44
#define MF2MICRO 45
#define MF1EN 46


AccelStepper MZ1 = AccelStepper(1, MZ1STEP, MZ1DIR);
AccelStepper MZ2 = AccelStepper(1, MZ2STEP, MZ2DIR);
AccelStepper MZ3 = AccelStepper(1, MZ3STEP, MZ3DIR);
AccelStepper MZ4 = AccelStepper(1, MZ4STEP, MZ4DIR);
AccelStepper MX1 = AccelStepper(1, MX1STEP, MX1DIR);
AccelStepper MX2 = AccelStepper(1, MX2STEP, MX2DIR);
AccelStepper MY1 = AccelStepper(1, MY1STEP, MY1DIR);
AccelStepper MY2 = AccelStepper(1, MY2STEP, MY2DIR);
AccelStepper MF1 = AccelStepper(1, MF1STEP, MF1DIR);
AccelStepper MF2 = AccelStepper(1, MF2STEP, MF2DIR);

MultiStepper steppers;


void setup() {
  MZ1.setMaxSpeed(50000);
  MZ1.setAcceleration(1000);
  MZ2.setMaxSpeed(50000);
  MZ2.setAcceleration(1000);
  MZ3.setMaxSpeed(50000);
  MZ3.setAcceleration(1000);
  MZ4.setMaxSpeed(50000);
  MZ4.setAcceleration(1000);
  
  steppers.addStepper(MZ1);
  steppers.addStepper(MZ2);
  steppers.addStepper(MZ3);
  steppers.addStepper(MZ4);
  
  pinMode(MZ1MICRO, OUTPUT);
  pinMode(MZ2MICRO, OUTPUT);
  pinMode(MZ3MICRO, OUTPUT);
  pinMode(MZ4MICRO, OUTPUT);
  digitalWrite(MZ1MICRO, HIGH);
  digitalWrite(MZ2MICRO, HIGH);
  digitalWrite(MZ3MICRO, HIGH);
  digitalWrite(MZ4MICRO, HIGH);
}


void loop() {
  int value = 10000;
  
  long positions[4];
  positions[0] = value;
  positions[1] = value;
  positions[2] = value;
  positions[3] = value;
  steppers.moveTo(positions);
  steppers.runSpeedToPosition();
  
  delay(1000);
  
  positions[0] = 0;
  positions[1] = 0;
  positions[2] = 0;
  positions[3] = 0;
  
  steppers.moveTo(positions);
  steppers.runSpeedToPosition();
  delay(1000);
}
