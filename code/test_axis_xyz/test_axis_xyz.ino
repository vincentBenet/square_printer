#include <AccelStepper.h>
#include <MultiStepper.h>

// Pole 4
#define MZ4MICRO 25
#define MX2EN 51
#define MZ4EN 50
#define MX2STEP 6
#define MZ4STEP 7
#define MX2DIR 47
#define MZ4DIR 48
#define MX2MICRO 49

// Pole 3
#define MZ3MICRO 60
#define MY2EN 61
#define MZ3EN 62
#define MY2STEP 10
#define MZ3STEP 11
#define MY2DIR 36
#define MZ3DIR 64
#define MY2MICRO 63

// Pole 2
#define MZ2MICRO 38
#define MX1EN 65
#define MZ2EN 66
#define MX1STEP 8
#define MZ2STEP 9
#define MX1DIR 40
#define MZ2DIR 52
#define MX1MICRO 53

// Pole 1
#define MZ1MICRO 24
#define MY1EN 2
#define MZ1EN 3
#define MY1STEP 12
#define MZ1STEP 13
#define MY1DIR 26
#define MZ1DIR 59
#define MY1MICRO 58

// Hotend
#define MF2EN 23
#define TQHBUSE 34
#define TQVBUSE 32
#define TQVRAD 30
#define THBUSE 68
#define MF1STEP 5
#define MF2STEP 4
#define MF1DIR 22
#define MF2DIR 42
#define MF1MICRO 44
#define MF2MICRO 45
#define MF1EN 46


AccelStepper MZ1 = AccelStepper(1, MZ1STEP, MZ1DIR);
AccelStepper MZ2 = AccelStepper(1, MZ2STEP, MZ2DIR);
AccelStepper MZ3 = AccelStepper(1, MZ3STEP, MZ3DIR);
AccelStepper MZ4 = AccelStepper(1, MZ4STEP, MZ4DIR);
AccelStepper MX1 = AccelStepper(1, MX1STEP, MX1DIR);
AccelStepper MX2 = AccelStepper(1, MX2STEP, MX2DIR);
AccelStepper MY1 = AccelStepper(1, MY1STEP, MY1DIR);
AccelStepper MY2 = AccelStepper(1, MY2STEP, MY2DIR);
AccelStepper MF1 = AccelStepper(1, MF1STEP, MF1DIR);
AccelStepper MF2 = AccelStepper(1, MF2STEP, MF2DIR);


MultiStepper steppers;


void setup() {
    
  MX1.setMaxSpeed(1000);
  MX1.setAcceleration(50);
  MX2.setMaxSpeed(1000);
  MX2.setAcceleration(50);
  MY1.setMaxSpeed(1000);
  MX1.setAcceleration(50);
  MX2.setMaxSpeed(1000);
  MX2.setAcceleration(50);
  MY1.setMaxSpeed(1000);
  MY1.setAcceleration(50);
  MY2.setMaxSpeed(1000);
  MY2.setAcceleration(50);
  MZ1.setMaxSpeed(50000);
  MZ1.setAcceleration(1000);
  MZ2.setMaxSpeed(50000);
  MZ2.setAcceleration(1000);
  MZ3.setMaxSpeed(50000);
  MZ3.setAcceleration(1000);
  MZ4.setMaxSpeed(50000);
  MZ4.setAcceleration(1000);
  
  steppers.addStepper(MX1);
  steppers.addStepper(MX2);
  steppers.addStepper(MY1);
  steppers.addStepper(MY2);
  steppers.addStepper(MZ1);
  steppers.addStepper(MZ2);
  steppers.addStepper(MZ3);
  steppers.addStepper(MZ4);

  pinMode(MZ1MICRO, OUTPUT);
  pinMode(MZ2MICRO, OUTPUT);
  pinMode(MZ3MICRO, OUTPUT);
  pinMode(MZ4MICRO, OUTPUT);
  digitalWrite(MZ1MICRO, HIGH);
  digitalWrite(MZ2MICRO, HIGH);
  digitalWrite(MZ3MICRO, HIGH);
  digitalWrite(MZ4MICRO, HIGH);
}

void move_motor(MultiStepper stepper, int value) {
  
}

void loop() {
  int value_xy = 1000;
  int value_z = 10000;
  
  long positions[8];
  positions[0] = value_xy;
  positions[1] = -value_xy;
  positions[2] = value_xy;
  positions[3] = -value_xy;
  positions[4] = value_z;
  positions[5] = value_z;
  positions[6] = value_z;
  positions[7] = value_z;
  
  steppers.moveTo(positions);
  steppers.runSpeedToPosition();
  
  delay(1000);
  
  positions[0] = 0;
  positions[1] = 0;
  positions[2] = 0;
  positions[3] = 0;
  positions[4] = 0;
  positions[5] = 0;
  positions[6] = 0;
  positions[7] = 0;
  
  steppers.moveTo(positions);
  steppers.runSpeedToPosition();
  delay(1000);
}
