#define TQHBUSE 34
#define THBUSE A14

void setup() {
  Serial.begin(9600);
  pinMode(THBUSE, INPUT_PULLUP);


}

void loop() {
  int sensorValue = analogRead(THBUSE);
  Serial.print("T°: ");
  Serial.println(sensorValue);
}
