#include <AccelStepper.h>

// Pole 4
#define MZ4MICRO 25
#define MX2EN 51
#define MZ4EN 50
#define EY1 57
#define MX2STEP 6
#define MZ4STEP 7
#define MX2DIR 47
#define MZ4DIR 48
#define MX2MICRO 49

// Pole 3
#define MZ3MICRO 60
#define MY2EN 61
#define MZ3EN 62
#define EX2 55
#define MY2STEP 10
#define MZ3STEP 11
#define MY2DIR 36
#define MZ3DIR 64
#define MY2MICRO 63

// Pole 2
#define MZ2MICRO 38
#define MX1EN 65
#define MZ2EN 66
#define EY2 56
#define MX1STEP 8
#define MZ2STEP 9
#define MX1DIR 40
#define MZ2DIR 52
#define MX1MICRO 53

// Pole 1
#define MZ1MICRO 24
#define MY1EN 2
#define MZ1EN 3
#define EX1 54
#define MY1STEP 12
#define MZ1STEP 13
#define MY1DIR 26
#define MZ1DIR 59
#define MY1MICRO 58

// Hotend
#define MF2EN 23
#define TQHBUSE 34
#define TQVBUSE 32
#define TQVRAD 30
#define THBUSE 68
#define EZ 69
#define MF1STEP 5
#define MF2STEP 4
#define MF1DIR 22
#define MF2DIR 42
#define MF1MICRO 44
#define MF2MICRO 45
#define MF1EN 46

// Bed
#define TQHBED 28
#define THBED 67

AccelStepper MZ1 = AccelStepper(1, MZ1STEP, MZ1DIR);
AccelStepper MZ2 = AccelStepper(1, MZ2STEP, MZ2DIR);
AccelStepper MZ3 = AccelStepper(1, MZ3STEP, MZ3DIR);
AccelStepper MZ4 = AccelStepper(1, MZ4STEP, MZ4DIR);
AccelStepper MX1 = AccelStepper(1, MX1STEP, MX1DIR);
AccelStepper MX2 = AccelStepper(1, MX2STEP, MX2DIR);
AccelStepper MY1 = AccelStepper(1, MY1STEP, MY1DIR);
AccelStepper MY2 = AccelStepper(1, MY2STEP, MY2DIR);
AccelStepper MF1 = AccelStepper(1, MF1STEP, MF1DIR);
AccelStepper MF2 = AccelStepper(1, MF2STEP, MF2DIR);

void setup() {
  MZ1.setMaxSpeed(9999999);
  MZ1.setAcceleration(100000);
  MZ2.setMaxSpeed(9999999);
  MZ2.setAcceleration(100000);
  MZ3.setMaxSpeed(9999999);
  MZ3.setAcceleration(100000);
  MZ4.setMaxSpeed(9999999);
  MZ4.setAcceleration(100000);
  MX1.setMaxSpeed(9999999);
  MX1.setAcceleration(100000);
  MX2.setMaxSpeed(9999999);
  MX2.setAcceleration(100000);
  MY1.setMaxSpeed(9999999);
  MY1.setAcceleration(100000);
  MY2.setMaxSpeed(9999999);
  MY2.setAcceleration(100000);
  MF1.setMaxSpeed(9999999);
  MF1.setAcceleration(100000);
  MF2.setMaxSpeed(9999999);
  MF2.setAcceleration(100000);
  
  // ENABLE PINS MODS
  pinMode(MZ1MICRO, OUTPUT);
  pinMode(MZ2MICRO, OUTPUT);
  pinMode(MZ3MICRO, OUTPUT);
  pinMode(MZ4MICRO, OUTPUT);
  pinMode(MX1MICRO, OUTPUT);
  pinMode(MX2MICRO, OUTPUT);
  pinMode(MY1MICRO, OUTPUT);
  pinMode(MY2MICRO, OUTPUT);
  pinMode(MF1MICRO, OUTPUT);
  pinMode(MF2MICRO, OUTPUT);
  
  pinMode(MZ1EN, OUTPUT);
  pinMode(MZ2EN, OUTPUT);
  pinMode(MZ3EN, OUTPUT);
  pinMode(MZ4EN, OUTPUT);
  pinMode(MX1EN, OUTPUT);
  pinMode(MX2EN, OUTPUT);
  pinMode(MY1EN, OUTPUT);
  pinMode(MY2EN, OUTPUT);
  pinMode(MF1EN, OUTPUT);
  pinMode(MF2EN, OUTPUT);

  pinMode(MZ1DIR, OUTPUT);
  pinMode(MZ2DIR, OUTPUT);
  pinMode(MZ3DIR, OUTPUT);
  pinMode(MZ4DIR, OUTPUT);
  pinMode(MX1DIR, OUTPUT);
  pinMode(MX2DIR, OUTPUT);
  pinMode(MY1DIR, OUTPUT);
  pinMode(MY2DIR, OUTPUT);
  pinMode(MF1DIR, OUTPUT);
  pinMode(MF2DIR, OUTPUT);

  pinMode(MZ1STEP, OUTPUT);
  pinMode(MZ2STEP, OUTPUT);
  pinMode(MZ3STEP, OUTPUT);
  pinMode(MZ4STEP, OUTPUT);
  pinMode(MX1STEP, OUTPUT);
  pinMode(MX2STEP, OUTPUT);
  pinMode(MY1STEP, OUTPUT);
  pinMode(MY2STEP, OUTPUT);
  pinMode(MF1STEP, OUTPUT);
  pinMode(MF2STEP, OUTPUT);

  pinMode(TQHBUSE, OUTPUT);
  pinMode(TQVBUSE, OUTPUT);
  pinMode(TQVRAD, OUTPUT);
  pinMode(TQHBED, OUTPUT);

  pinMode(EX1, INPUT);
  pinMode(EX2, INPUT);
  pinMode(EY1, INPUT);
  pinMode(EY2, INPUT);
  pinMode(EZ, INPUT);

  pinMode(THBUSE, INPUT);
  pinMode(THBED, INPUT);

  // Enable all motors
  digitalWrite(MZ1EN, LOW);
  digitalWrite(MZ2EN, LOW);
  digitalWrite(MZ3EN, LOW);
  digitalWrite(MZ4EN, LOW);
  digitalWrite(MX1EN, LOW);
  digitalWrite(MX2EN, LOW);
  digitalWrite(MY1EN, LOW);
  digitalWrite(MY2EN, LOW);
  digitalWrite(MF1EN, LOW);
  digitalWrite(MF2EN, LOW);

  // Microstepping
  digitalWrite(MZ1MICRO, HIGH);
  digitalWrite(MZ2MICRO, HIGH);
  digitalWrite(MZ3MICRO, HIGH);
  digitalWrite(MZ4MICRO, HIGH);
  digitalWrite(MX1MICRO, HIGH);
  digitalWrite(MX2MICRO, HIGH);
  digitalWrite(MY1MICRO, HIGH);
  digitalWrite(MY2MICRO, HIGH);
  digitalWrite(MF1MICRO, HIGH);
  digitalWrite(MF2MICRO, HIGH);
  digitalWrite(MX1MICRO, HIGH);
  digitalWrite(MX2MICRO, HIGH);
  digitalWrite(MY1MICRO, HIGH);
  digitalWrite(MY2MICRO, HIGH);
}

void move_motor(AccelStepper stepper, int value) {
  stepper.moveTo(value);
  stepper.runToPosition();
  stepper.moveTo(0);
  stepper.runToPosition();
}

void loop() {
  move_motor(MZ1, 500);
  move_motor(MZ2, 500);
  move_motor(MZ3, 500);
  move_motor(MZ4, 500);
  move_motor(MX1, 500);
  move_motor(MX2, 500);
  move_motor(MY1, 500);
  move_motor(MY2, 500);
  move_motor(MF1, 500);
  move_motor(MF2, 500);
}
