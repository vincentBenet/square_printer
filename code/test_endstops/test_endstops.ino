#define EX1 54
#define EX2 55
#define EY2 56
#define EY1 57
#define EZ1 69

void setup() {
  pinMode(EX1, INPUT);
  pinMode(EX2, INPUT);
  pinMode(EY1, INPUT);
  pinMode(EY2, INPUT);
  Serial.begin(9600);
}

void loop() {
  bool state_EX1 = !digitalRead(EX1);
  bool state_EX2 = !digitalRead(EX2);
  bool state_EY1 = !digitalRead(EY1);
  bool state_EY2 = !digitalRead(EY2);
  bool state_EZ1 = digitalRead(EZ1);

  if (state_EX1){Serial.print("EX1\n");};
  if (state_EX2){Serial.print("EX2\n");};
  if (state_EY1){Serial.print("EY1\n");};
  if (state_EY2){Serial.print("EY2\n");};
  if (state_EZ1){Serial.print("EZ1\n");};
}
