#include <AccelStepper.h>

// Pole 4
#define MZ4MICRO 25
#define MX2EN 51
#define MZ4EN 50
#define EY1 57
#define MX2STEP 6
#define MZ4STEP 7
#define MX2DIR 47
#define MZ4DIR 48
#define MX2MICRO 49

// Pole 3
#define MZ3MICRO 60
#define MY2EN 61
#define MZ3EN 62
#define EX2 55
#define MY2STEP 10
#define MZ3STEP 11
#define MY2DIR 36
#define MZ3DIR 64
#define MY2MICRO 63

// Pole 2
#define MZ2MICRO 38
#define MX1EN 65
#define MZ2EN 66
#define EY2 56
#define MX1STEP 8
#define MZ2STEP 9
#define MX1DIR 40
#define MZ2DIR 52
#define MX1MICRO 53

// Pole 1
#define MZ1MICRO 24
#define MY1EN 2
#define MZ1EN 3
#define EX1 54
#define MY1STEP 12
#define MZ1STEP 13
#define MY1DIR 26
#define MZ1DIR 59
#define MY1MICRO 58

// Hotend
#define MF2EN 23
#define TQHBUSE 34
#define TQVBUSE 32
#define TQVRAD 30
#define THBUSE 68
#define EZ 69
#define MF1STEP 5
#define MF2STEP 4
#define MF1DIR 22
#define MF2DIR 42
#define MF1MICRO 44
#define MF2MICRO 45
#define MF1EN 46

// Bed
#define TQHBED 28
#define THBED 67

// Endstops
#define EX1 54
#define EX2 55
#define EY2 56
#define EY1 57

void setup() {

}


void loop() {
}
