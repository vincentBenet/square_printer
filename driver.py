#!/usr/bin/env python2

import sys, socket, json, errno, time, re


class API:
    
    def __init__(self, uds_filename):
        self.uds_filename = uds_filename
        self.socket_data = ""
        
        self.connect()
        self.enable_outputs()
        self.get_firmware_version()
        self.get_temperature()
        self.get_position()
        # self.get_positions()
        self.bed_mesh()
        
    def connect(self):
        self.sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        sys.stderr.write("Waiting for connect to %s\n" % (self.uds_filename,))
        while 1:
            try:
                self.sock.connect(self.uds_filename)
            except socket.error as e:
                if e.errno == errno.ECONNREFUSED:
                    time.sleep(0.1)
                    continue
                sys.stderr.write("Unable to connect socket %s [%d,%s]\n"
                                 % (self.uds_filename, e.errno,
                                    errno.errorcode[e.errno]))
                sys.exit(-1)
            break
        print("CONNECTED")
    
    def send(self, message):
        self.sock.send("%s\x03" % (message))
    
    def get_answer(self, no_temp=True):
        for message in self.get_message():
            if not len(message):
                continue
            sys.stdout.write('\n' + message + '\n')
            res = 1
        return res

    def get_message(self):
        result = []
        try:
            data = self.sock.recv(4096)
            for message in data.split('\x03'):
                try:
                    output = str(json.loads(message).get("params", {}).get("response", ""))
                    result.append(output)
                except ValueError:
                    continue
        except KeyboardInterrupt:
            sys.exit(-1)
        except socket.error:
            self.connect()
        return result
    
    def gcode(self, code, wait_begin=False, wait_end=True, wait_answer=True, timeout=0):
        command_type = code.split(" ")[0]
        command_name = gcodes.get(command_type, {}).get("description", command_type)
        if code != "M400" and wait_begin:
            self.gcode("M400")
        if code != "M400": print(">GCODE " + code + " ("+command_name+")")
        self.send('{"params":{"script":"' + code + '"},"id":123,"method":"gcode/script"}')
        if code != "M400" and wait_end:
            self.gcode("M400")
        if hasattr(self, command_type) and wait_answer:
            callable = getattr(self, command_type)
            t0 = time.time()
            while 1:
                output = [l for l in self.get_message() if l]
                if output not in [[], [""]] and not(re.compile(regexes['Temperature']).search(output[0])): print(">GET " + repr(output))
                if (
                    "// Klipper state: Shutdown" in output or 
                    '// MCU \'mcu\' shutdown: Rescheduled timer in the past\n// This generally occurs when the micro-controller has been\n// requested to step at a rate higher than it is capable of\n// obtaining.\n// Once the underlying issue is corrected, use the\n// "FIRMWARE_RESTART" command to reset the firmware, reload the\n// config, and restart the host software.\n// Printer is shutdown' in output or
                    "!! MCU 'mcu' shutdown: Rescheduled timer in the past" in output
                ):
                    print("Internal Error (speed to high?)")
                    self.restart()
                    return gcode(self, code, wait_begin=wait_begin, wait_end=wait_end, wait_answer=wait_answer, timeout=timeout)
                result = callable(output)
                if timeout > 0 and time.time() - t0 > timeout:
                    raise Exception("Timeout for GCODE answer "+code)
                if result is not None:
                    break
            return result
        
    def enable_outputs(self):
        self.send('{"params":{"response_template":{"key":345}},"id":123,"method":"gcode/subscribe_output"}')

########################################################################################################

    def BED_MESH_OUTPUT(self, output):
        for content in output:
            data = content.split("\nMeasured points:\n")
            if len(data) == 2:
                numbers = data[-1]
                result = []
                for line in numbers.split("\n"):
                    result_line = []
                    for number in line.split("  "):
                        try:
                            value = float(number)
                        except ValueError:
                            continue
                        result_line.append(value)
                    if len(result_line):
                        result.append(result_line)
                return result
    
    def BED_MESH_CALIBRATE(self, output):
        for content in output:
            for stop_message in [
                "!! Must home axis first: ",
                "!! Probe triggered prior to movement",
                "// Mesh Bed Leveling Complete",
                "// Bed Mesh state has been saved to profile",
            ]:
                if content.startswith(stop_message):
                    return content
            for continue_message in [
                "// probe at",
            ]:
                if content.startswith(continue_message): 
                    continue
    
    def G28(self, output):  # Homing
        for content in output:
            for stop_message in [
                "!! No trigger on x after full movement",
                "!! Endstop z still triggered after retract"
            ]:
                if content.startswith(stop_message):
                    return content
    
    def M115(self, output):  # Get firmware version
        for content in output:
            if content.startswith("// FIRMWARE_VERSION:"):
                return content.replace("// FIRMWARE_VERSION:", "").split(" FIRMWARE_NAME:")[0]
    
    def M114(self, output):  # Get position
        for content in output:
            if re.compile(regexes['M114']).search(content):
                x = float(content.split(" Y:")[0][2:])
                y = float(content.split(" Y:")[1].split(" Z:")[0])
                z = float(content.split(" Y:")[1].split(" Z:")[1].split(" E:")[0])
                e = float(content.split(" E:")[1])
                return x, y, z, e
    
    
    def M105(self, output):  # Get position
        for content in output:
            if re.compile(regexes['M105']).search(content):
                t_buse = float(content.split(" /0.0 T0:")[0][2:])
                t_bed = float(content.split(" /0.0 T0:")[1].split(" ")[0][2:])
                return t_buse, t_bed
    
    def GET_POSITION(self, output):
        for content in output:
            pass
            # TODO
    
########################################################################################################
    def bed_mesh(self):
        answer = self.gcode("BED_MESH_CALIBRATE")
        
        for retry_message in [
            "!! Must home axis first: ",
            "!! Probe triggered prior to movement",
        ]:
            if answer.startswith(retry_message):
                self.homing()
                return self.bed_mesh()
        for finish_message in [
            "// Mesh Bed Leveling Complete",
            "// Bed Mesh state has been saved to profile",
        ]:
            if answer.startswith(retry_message):
                self.gcode("SAVE_CONFIG")
    
    def homing(self, x=True, y=True, z=True):
        command = "G28"
        if x: command += " X"
        if y: command += " Y"
        if z: command += " Z"
        answer = self.gcode(command)
        
        for error_message in [
            "!! No trigger on x after full movement",
            "!! Endstop z still triggered after retract",
        ]:
            if answer.startswith(error_message):
                raise Exception(error_message)
    
    def move_stepper(self, axis, num=0, distance=10, speed=10, accel=10):
        if axis not in "xyz":
            raise Exception("Wrong axis name")
        if not isinstance(num, int) or num < 0:
            raise Exception("Wrong stepper value")
        stepper_name = "stepper_" + axis
        if num > 1:
            stepper_name += str(num - 1)
        self.gcode(
            "FORCE_MOVE STEPPER="+stepper_name+" "+
            "DISTANCE="+str(distance)+" "+
            "VELOCITY="+str(speed)+" "+
            "ACCEL="+str(accel),
            wait_answer= False,
            wait_begin=True, 
            wait_end=True
        )
    
    def goto(self, x=None, y=None, z=None, speed=10):
        command = "G1"
        if x is not None:
            if not isintance(x, float) or isintance(x, int) and x > 0:
                raise Exception("X value is not valid")
            command += " X" + str(x)
        if y is not None:
            if not isintance(y, float) or isintance(y, int) and y > 0:
                raise Exception("Y value is not valid")
            command += " Y" + str(y)
        if z is not None:
            if not isintance(z, float) or isintance(z, int) and z > 0:
                raise Exception("Z value is not valid")
            command += " Z" + str(z)
        self.gcode(command)
    
    
    def get_firmware_version(self):
        firmware_version = self.gcode("M115")
        print("firmware_version: " + firmware_version)
        return firmware_version
        
    def calibrate_bed(self):
        return self.gcode("BED_MESH_CALIBRATE")
    
    def restart(self):
        self.gcode("FIRMWARE_RESTART", wait_answer=False, timeout=1, wait_end=False)
    
    def get_position(self):
        x, y, z, e = self.gcode("M114")
        print("Position: " + str([x, y, z, e]))
        return x, y, z, e
    
    def get_temperature(self):
        t_buse, t_bed = self.gcode("M105")
        print("Temperatures: Buse: " + str(t_buse) + "C - Bed: " + str(t_bed )+ "C")
        return t_buse, t_bed
    
    def test_motor(self, axis, num=0):
        stepper_name = "stepper_" + axis
        if num > 1:
            stepper_name += str(num - 1)
        self.gcode("STEPPER_BUZZ STEPPER=" + stepper_name)
    
    def get_positions(self):
        self.gcode("GET_POSITION")

be

regexes = {
    "Temperature": r'^ok B:[0-9]*.[0-9]* \/[0-9]*.[0-9]* T0:[0-9]*.[0-9]* \/[0-9]*.[0-9]*$',
    "M114": r"^X:(.{0}|-)[0-9]+.[0-9]+ Y:(.{0}|-)[0-9]+.[0-9]+ Z:(.{0}|-)[0-9]+.[0-9]+ E:(.{0}|-)[0-9]+.[0-9]+$",
    "M105": r'^B:[0-9]*.[0-9]* \/[0-9]*.[0-9]* T0:[0-9]*.[0-9]* \/[0-9]*.[0-9]*$',
    "BedMeshOutput": r'^Mesh X,Y: (.{0}|-)[0-9]+,[0-9]+\\nSearch Height: (.{0}|-)[0-9]+\\nMesh Offsets: X=(.{0}|-)[0-9]+.[0-9]+, Y=(.{0}|-)[0-9]+.[0-9]+\\nMesh Average: (.{0}|-)[0-9]+.[0-9]+\\nMesh Range: min=(.{0}|-)[0-9]+.[0-9]+ max=(.{0}|-)[0-9]+.[0-9]+\\nInterpolation Algorithm: [a-zA-Z\s]*\\nMeasured points:\\n  (.*)$',
}

gcodes = {
    "BED_MESH_CALIBRATE": {
        "description": "Bed calibration",
        "behavior": {
            "retry": {
                "regex": [
                    "^!! Must home axis first: .*$",
                    "^!! Probe triggered prior to movement$",
                ],
                "functions": [
                    self.homing,
                    self.bed_mesh,
                ],
            },
            "finish": {
                "regex": [
                    "^// Mesh Bed Leveling Complete$",
                    "^// Bed Mesh state has been saved to profile.*$",
                ],
                "functions": [
                    lambda: self.gcode("SAVE_CONFIG")
                ],
            }
        }
    },
    "G28": {
        "description": "Homing axis",
    },
    "M114": "Get position XYZE",
    "BED_MESH_CALIBRATE": "Bed calibration",
    "FORCE_MOVE": "Moving only one motor",
}

if __name__ == "__main__":
    API("/tmp/klippy_uds")
