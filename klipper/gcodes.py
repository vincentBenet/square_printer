import parse


gcodes = {
    "FIRMWARE_RESTART": {
        "description": "Restarting the printer firmware",
        "behavior": {
            "succes": {
                "regex": [
                    "^// Klipper state: Ready$",
                    "^ok .*$",
                ],
            },
        },
    },
    "G28": {"description": "Homing",
        "behavior": {
            "fail no movement": {
                "regex": [
                    "^!! No trigger on (x|y|z) after full movement$",
                ],
                "functions": [
                    lambda : (_ for _ in ()).throw(Exception("Turn On main alimentation"))
                ],
            },
            "fail probe retracted": {
                "regex": [
                    "^!! Endstop (x|y|z) still triggered after retract$",
                ],
                "functions": [
                    lambda : (_ for _ in ()).throw(Exception("Probe blocked"))
                ],
            },
            "finish": {
                "regex": [
                    "^ok B:[0-9]*.[0-9]* \/[0-9]*.[0-9]* T0:[0-9]*.[0-9]* \/[0-9]*.[0-9]*$"
                ],
                "functions": [
                    lambda: True
                ],
            },
        },
    },
    "M105": {"description": "Get temperatures",
        "function_parse": parse.parse_temperatures,
    },
    "BED_MESH_OUTPUT": {"description": "Get bed level",
        "function_parse": parse.parse_bed_mesh_output, 
    },
    "BED_MESH_CALIBRATE": {"description": "Bed calibration",
        "behavior": {
            "retry": {
                "regex": [
                    "^!! Must home axis first: .*$",
                    "^!! Probe triggered prior to movement$",
                    "^!! Probing failed due to printer shutdown$",
                    "^!! No trigger on probe after full movement$",
                ],
                "functions": [
                    "G28 X",
                    "G28 Y",
                    "G28 Z",
                    "BED_MESH_CALIBRATE",
                ],
            },
            "continue": {
                "regex": [
                    "^// probe at.*$",
                ],
            },
            "restart": {
                "regex": [
                    "^Probing failed due to printer shutdown$",
                ],
                "functions": [
                    "FIRMWARE_RESTART",
                ],
            },
            "finish": {
                "regex": [
                    "^// Mesh Bed Leveling Complete$",
                    "^// Bed Mesh state has been saved to profile.*$",
                ],
                "functions": [
                    "SAVE_CONFIG",
                    "BED_MESH_OUTPUT",
                ],
            },
        },
    },
    "M115": {"description": "Get firmware version",
        "function_parse": lambda answer: [
            content.replace("// FIRMWARE_VERSION:", "").split(" FIRMWARE_NAME:")[0] 
            for content in answer 
            if content.startswith("// FIRMWARE_VERSION:")
        ][0],
    },
    "M400": {"description": "Wait end of command",
    },
    "M114": {"description": "Get positions X, Y, Z, E",
        "function_parse": parse.parse_positions_xyze,
    },
    "FORCE_MOVE": {"description": "Move a specific motor",
        
    },
    "STEPPER_BUZZ": {"description": "Buzz a specific motor",
    },
    "G1": {"description": "Go to specific coordinates",
    },
    "M18": {"description": "Release motors",
    },
}
