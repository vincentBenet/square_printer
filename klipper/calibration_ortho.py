"""
Script de calcul du defaut d'orthogonalité à partir de la mesure d'un plan incliné connu
Ce script simule un defaut d'orthogonalité puis le retrouve à partir de la mesure

"""

import math
import numpy
import matplotlib.pyplot as plt
import scipy


def generate_grid(
    largeur: float,
    longeur: float,
    nx: int,
    ny: int,
    value: float = 0
) -> numpy.ndarray:
    """
    Create a set of points of a plan perpendicular to Z axis (vertical)

    :param largeur: Size of grid on X axis in meter
    :param longeur: Size of grid on Y axis in meter
    :param nx: Number of points on X axis
    :param ny: Number of points on Y axis
    :param value: Initial values on z axis
    :return: List of list of 3D points [[x1, y1, z1], [x2, y2, z2], ...]
    """

    assert largeur > 0
    assert longeur > 0
    assert nx > 2
    assert ny > 2

    grid_x, grid_y = numpy.meshgrid(
        numpy.linspace(0, largeur, nx) - largeur / 2,
        numpy.linspace(0, longeur, ny) - longeur / 2
    )
    return numpy.array([
        grid_x.flatten(),
        grid_y.flatten(),
        numpy.ones((ny, nx)).flatten() * value
    ]).T


def apply_mat_3d_points(
    pts: numpy.ndarray,
    mat: numpy.ndarray
) -> numpy.ndarray:
    """
    Apply a transformation matrix on a 3D set of points

    :param pts: Liste de liste de points [[x1, y1, z1], [x2, y2, z2], ...] en input
    :param mat: Matrice 3x3 de transformation des points
    :return: Liste de liste de points [[x1, y1, z1], [x2, y2, z2], ...] transformé de la matrice
    """
    dim_x_mat, dim_y_mat = numpy.shape(mat)
    dim_x_pts, dim_y_pts = numpy.shape(mat)

    assert dim_x_mat == 3
    assert dim_y_mat == 3
    assert dim_y_pts == 3

    return numpy.dot(mat, pts.T).T


def fun(
    p: numpy.ndarray,
    ptsm: numpy.ndarray,
    largeur_mesure: float,
    longeur_mesure: float,
    nx_mesure: int,
    ny_mesure: int,
    largeur_vis: float,
    longeur_vis: float,
    frx: float,
    fry: float,
) -> float:
    """
    Fonction de score a minimiser pour trouver le defaut d'orthogonalité entre l'axe X et l'axe Y.

    :param p: Guess on orthogonality error (dx, dy).
    :param ptsm: Actual measurment of the grid (with orthogonality error)
    :param largeur_mesure: Size of grid on X axis in meter
    :param longeur_mesure: Size of grid on Y axis in meter
    :param nx_mesure: Number of points of measurment on X axis
    :param ny_mesure: Number of points of measurment on Y axis
    :param largeur_vis: Distance of axis driving on X in meter
    :param longeur_vis: Distance of axis driving on Y in meter
    :param frx: Forced angle of the bed around X axis in rad
    :param fry: Forced angle of the bed around Y axis in rad
    :return: Score of the guess on orthogonality error
    """
    dx, dy = p  # Parameters extraction (obligation of minimize method)
    rotation_mat = numpy.dot(  # Create rotation matrix around X and Y
        numpy.array([  # Rotation around X axis
            [1, 0, 0],
            [0, numpy.cos(frx), -numpy.sin(frx)],
            [0, numpy.sin(frx), numpy.cos(frx)]
        ]),
        numpy.array([  # Rotation around Y axis
            [numpy.cos(fry), 0, numpy.sin(fry)],
            [0, 1, 0],
            [-numpy.sin(fry), 0, numpy.cos(fry)]
        ])
    )
    rzx = math.atan(dx / largeur_vis)  # Angle of orthogonality error of x axis
    rzy = math.atan(dy / longeur_vis)  # Angle of orthogonality error of y axis
    deformation_mat = numpy.array([  # Creation of transformation matrix for non-orthogonal system
        [numpy.cos(rzx), numpy.sin(rzx), 0],  # X axis rotation
        [-numpy.sin(rzy), numpy.cos(rzy), 0],  # Y axis rotation
        [0, 0, 1]  # Same values on Z (only planar transformation)
    ])

    pts = generate_grid(largeur_mesure, longeur_mesure, nx_mesure, ny_mesure)
    ptsi = apply_mat_3d_points(pts, rotation_mat)
    ptsd = apply_mat_3d_points(pts, deformation_mat)
    ptsdi = apply_mat_3d_points(ptsd, rotation_mat)
    ptsms = numpy.array([ptsi.T[0], ptsi.T[1], ptsdi.T[2]]).T

    return float(numpy.sum(numpy.square(ptsms - ptsm)))


def calcul(*args):
    """
    Research of orthogonality error using the minimisation of error function with measured data.

    :param args: Arguments to transfer to minimize function
    :return: Calculated error of orthogonality on X and Y axis.
    """
    return scipy.optimize.minimize(
        fun,  # Scoring function
        numpy.array([0, 0]),  # Initial guess of parameters
        args=args,  # Arguments of function to minimize
    ).x


if __name__ == "__main__":

    largeur_mesure = 0.3
    longeur_mesure = 0.4
    nx_mesure = 5
    ny_mesure = 10
    largeur_vis = 0.5
    longeur_vis = 0.45
    dx = 0.05
    dy = -0.03
    frx = numpy.pi / 10
    fry = numpy.pi / 5

    # Calculs
    rotation_mat = numpy.dot(
        numpy.array([[1, 0, 0], [0, numpy.cos(frx), -numpy.sin(frx)], [0, numpy.sin(frx), numpy.cos(frx)]]),
        numpy.array([[numpy.cos(fry), 0, numpy.sin(fry)], [0, 1, 0], [-numpy.sin(fry), 0, numpy.cos(fry)]])
    )
    rzx = math.atan(dx / largeur_vis)
    rzy = math.atan(dy / longeur_vis)
    deformation_mat = numpy.array([[numpy.cos(rzx), numpy.sin(rzx), 0], [-numpy.sin(rzy), numpy.cos(rzy), 0], [0, 0, 1]])

    # Main
    pts = generate_grid(largeur_mesure, longeur_mesure, nx_mesure, ny_mesure)
    ptsi = apply_mat_3d_points(pts, rotation_mat)
    ptsd = apply_mat_3d_points(pts, deformation_mat)
    ptsdi = apply_mat_3d_points(ptsd, rotation_mat)
    ptsm = numpy.array([ptsi.T[0], ptsi.T[1], ptsdi.T[2]]).T
    dxm, dym = calcul(ptsm, largeur_mesure, longeur_mesure, nx_mesure, ny_mesure, largeur_vis, longeur_vis, frx, fry)

    print(f'{dxm = }')
    print(f'{dym = }')

    # Display
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.scatter(*ptsi.T, color="red", label="Real position of plateau")
    ax.scatter(*ptsm.T, color="blue", label="Ce que je mesure")
    plt.show()
