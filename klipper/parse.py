import re


def parse_bed_mesh_calibrate(answer):
    for content in answer:
        if content.startswith("'// probe at"):
            return None
        if content.startswith("'ok"):
            return None
        
    else:
        return answer

def parse_bed_mesh_output(answer):
    for content in answer:
        data = content.split("\nMeasured points:\n")
        if len(data) == 2:
            numbers = data[-1]
            result = []
            for line in numbers.split("\n"):
                result_line = []
                for number in line.split("  "):
                    try:
                        value = float(number)
                    except ValueError:
                        continue
                    result_line.append(value)
                if len(result_line):
                    result.append(result_line)
            return result        


def parse_temperatures(answer):
    for content in answer:
        if re.compile(r'^B:[0-9]*.[0-9]* \/[0-9]*.[0-9]* T0:[0-9]*.[0-9]* \/[0-9]*.[0-9]*$').search(content):
            t_buse = float(content.split(" /0.0 T0:")[0][2:])
            t_bed = float(content.split(" /0.0 T0:")[1].split(" ")[0][2:])
            return t_buse, t_bed


def parse_positions_xyze(answer):
    for content in answer:
        if re.compile(r"^X:(.{0}|-)[0-9]+.[0-9]+ Y:(.{0}|-)[0-9]+.[0-9]+ Z:(.{0}|-)[0-9]+.[0-9]+ E:(.{0}|-)[0-9]+.[0-9]+$").search(content):
            x = float(content.split(" Y:")[0][2:])
            y = float(content.split(" Y:")[1].split(" Z:")[0])
            z = float(content.split(" Y:")[1].split(" Z:")[1].split(" E:")[0])
            e = float(content.split(" E:")[1])
            return x, y, z, e
