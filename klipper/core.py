import re
import communicate
from gcodes import gcodes

        
def gcode(code):
    """
    Run a specific GCODE command using the configuration file
    """
    command_params = gcodes.get(code.split(" ")[0], {})
    command_params["code"] = code
    if code != "M400" and command_params.get("wait_begin", True):
        gcode("M400")
    communicate.send_gcode(command_params)
    if code != "M400" and command_params.get("wait_end", True):
        gcode("M400")
    if code != "M400":
        return run(command_params)

def run(command_params):
    result = None
    behaviors = command_params.get("behavior", {})
    parsing_fnc = command_params.get("function_parse", lambda answer: answer)
    while True:
        output = []
        for line in communicate.get_message():
            print(">"+command_params["code"]+"\t\t"+"ANSWER:\t" + line)
            for restart_condition in [
                "// Klipper state: Shutdown",
                "// MCU 'mcu' shutdown: ADC out of range",
                "!! MCU 'mcu' shutdown: Rescheduled timer in the past",
                "// MCU \'mcu\' shutdown: Rescheduled timer in the past",
            ]:
                if line.startswith(restart_condition):
                    gcode("FIRMWARE_RESTART")
                    return gcode(command_params["code"])
            for continue_condition in [
                "// Klipper state: Disconnect",
                "!! Printer is not ready",
            ]:
                if line.startswith(restart_condition):
                    continue
            output.append(line)
        if not output:
            continue
        outputs = parsing_fnc(output)
        if outputs is None or not(outputs):
            continue
        print(">"+command_params["code"]+"\t\t"+"OUTPUTS:\t" + str(outputs))
        if isinstance(outputs, list) and len(behaviors):
            for result in outputs:
                for behavior in behaviors:
                    for regex in behaviors[behavior]["regex"]:
                        if re.compile(regex).search(result):
                            print(">\t\tBEHAVIOR:\t" + str(behavior))
                            res = None
                            functions = behaviors[behavior].get("functions", [])
                            print(">\t\tFUNCTIONS:\t" + str(functions))
                            for function in functions:
                                if isinstance(function, str):
                                    res = gcode(function)
                                else:
                                    res = function()
                            if res is not None:
                                print(">"+command_params["code"]+"\t\t"+"RESULT:\t" + repr(res))
                                return res
        else:
            print(">"+command_params["code"]+"\t\t"+"RESULT:\t" + repr(outputs))
            return outputs
