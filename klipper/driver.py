#!/usr/bin/python3

import time
from core import gcode
import communicate


def get_bed():
    return gcode("BED_MESH_CALIBRATE")


def force_move_safe(axis, distance, num=1, velocity=10, accel=10):
    gcode("G28 XY")
    gcode("G1 " + axis.upper() + str(abs(distance)))
    force_move(axis, distance, num=1, velocity=10, accel=10)
    
    
def force_move(axis, distance, num=1, velocity=10, accel=10):
    command = "FORCE_MOVE"
    command += " STEPPER=stepper_"
    command += axis
    if num > 0:
        command += str(num)
    command += " DISTANCE="
    command += str(distance)
    command += " VELOCITY="
    command += str(velocity)
    command += " ACCEL="
    command += str(accel)
    gcode(command)


def main():
    communicate.connect()
    version = gcode("M115")
    gcode("M18")
    bed = get_bed()
    
    time.sleep(5)
    force_move("z", 4, num=0)
    force_move("z", -4, num=0)
    force_move("z", 4, num=1)
    force_move("z", -4, num=1)
    force_move("z", 4, num=2)
    force_move("z", -4, num=2)
    force_move("z", 4, num=3)
    force_move("z", -4, num=3)
    
    
    

if __name__ == "__main__":
    main()
