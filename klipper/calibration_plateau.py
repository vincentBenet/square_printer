import scipy
import numpy
import random
import matplotlib.pyplot as plt


def get_z(a, b, c, d, e, f, g, x, y):
    return (
        # f * x**3 +
        # g * x**3 +
        a * x**2 +
        b * y**2 +
        c * x +
        d * y +
        e
    )


def fun(p, x, y, z):
    return sum(numpy.square(get_z(*p, x, y) - z))


def get_coefs_meansquare(X, Y, Z):
    x0 = numpy.array([
        0,
        0,
        0,
        0,
        0,
        0,
        0,
    ])
    return scipy.optimize.minimize(
        fun,
        x0,
        args=(X.flatten(), Y.flatten(), Z.flatten()),
    ).x


def generate_plateau_init(
    largeur,
    longeur,
    nx,
    ny,
    seed=0,
    noise=0.0
):

    random.seed(seed)
    a = random.random()
    b = random.random()
    c = random.random()
    d = random.random()
    e = random.random()
    f = random.random()
    g = random.random()

    print("Input")
    print(f"{a = }")
    print(f"{b = }")
    print(f"{c = }")
    print(f"{d = }")
    print(f"{e = }")
    print(f"{f = }")
    print(f"{g = }")

    grid_x, grid_y, dx, dy = get_grid_xy(longeur, largeur, nx, ny)

    return grid_x, grid_y, numpy.array([[random.random() * noise + get_z(a, b, c, d, e, f, g, i * dx, j * dy) for j in range(ny)] for i in range(nx)]).T,


def get_grid_xy(longeur, largeur, nx, ny):
    grid_x, grid_y = numpy.meshgrid(numpy.linspace(0, largeur, nx) - largeur / 2, numpy.linspace(0, longeur, ny) - longeur / 2)
    dx = largeur / nx
    dy = longeur / ny
    return grid_x, grid_y, dx, dy


if __name__ == "__main__":
    lon_mesure = 0.2
    lon_plateau = 0.50
    lar_mesure = 0.2
    lar_plateau = 0.45

    nxm = 6
    nym = int(nxm * lon_mesure / lar_mesure)
    nxp = int(nxm * lar_plateau/lar_mesure)
    nyp = int(nym * lon_plateau / lon_mesure)

    xm, ym, zm = generate_plateau_init(
        largeur=lar_mesure,
        longeur=lon_mesure,
        nx=nxm,
        ny=nym
    )
    
    zm = numpy.array([
        [-0.001250, -0.480000, -0.831250, -1.042500, -1.187500, -1.411250],
        [0.151250, -0.486250, -0.882500, -1.142500, -1.378750, -1.651250],
        [0.263750, -0.538750, -1.023750, -1.367500, -1.745000, -2.098750],
        [0.090000, -0.541250, -1.085000, -1.532500, -2.015000, -2.421250],
        [0.117500, -0.668750, -1.241250, -1.785000, -2.318750, -2.798750],
        [0.060000, -0.717500, -1.287500, -1.842500, -2.411250, -2.921250],
    ])/1000
    
    print(f"STD: {numpy.std(zm)}")
    print(f"VAR: {numpy.var(zm)}")
    print(f"MIN: {numpy.min(zm)}")
    print(f"MAX: {numpy.max(zm)}")
    print(f"MEDIAN: {numpy.median(zm)}")

    a, b, c, d, e, f, g = get_coefs_meansquare(xm, ym, zm)

    print("Output")
    print(f"{a = }")
    print(f"{b = }")
    print(f"{c = }")
    print(f"{d = }")
    print(f"{e = }")
    print(f"{f = }")

    xp, yp, _, _ = get_grid_xy(lon_plateau, lar_plateau, nxp, nyp)

    zp = get_z(a, b, c, d, e, f, g, xp, yp)

    z_ref = numpy.mean(zp)

    dz1 = zp[0][0] - z_ref
    dz2 = zp[0][-1] - z_ref
    dz3 = zp[-1][0] - z_ref
    dz4 = zp[-1][-1] - z_ref
    
    print()
    print(f"{dz1 = }")
    print(f"{dz2 = }")
    print(f"{dz3 = }")
    print(f"{dz4 = }")
    
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.plot_surface(xm, ym, zm, cmap='turbo')
    ax.plot_wireframe(xp, yp, zp, color="black", label="blaz")
    ax.plot_wireframe(xp, yp, numpy.ones((nxp, nyp)).T * z_ref, color="red")
    ax.quiver(
        numpy.array([
            xp[0][0],
            xp[0][-1],
            xp[-1][0],
            xp[-1][-1],
        ]), numpy.array([
            yp[0][0],
            yp[0][-1],
            yp[-1][0],
            yp[-1][-1],
        ]),
        numpy.array([
            zp[0][0],
            zp[0][-1],
            zp[-1][0],
            zp[-1][-1],
        ]), numpy.array([
            0,
            0,
            0,
            0,
        ]), numpy.array([
            0,
            0,
            0,
            0,
        ]), numpy.array([
            -dz1,
            -dz2,
            -dz3,
            -dz4,
        ]),
        color="blue",
        label=f"dz1={round(dz1*1000,1)}mm dz2={round(dz2*1000,1)}mm dz3={round(dz3*1000,1)}mm dz4={round(dz4*1000,1)}mm"
    )
    ax.legend()
    plt.show()
