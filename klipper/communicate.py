import sys, socket, json, errno, time


def connect(filename="/tmp/klippy_uds"):
    global sock
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    print("CONNECTING")
    while 1:
        try:
            sock.connect(filename)
        except socket.error as e:
            if e.errno == errno.ECONNREFUSED:
                time.sleep(1)
                print (".")
                continue
            sys.exit(-1)
        break
    print("\nCONNECTED")
    send('{"params":{"response_template":{"key":345}},"id":123,"method":"gcode/subscribe_output"}')


def send_gcode(command_params):
    code = command_params["code"]
    if not code.startswith("M400"):
        print(">\t\tGCODE:\t" + code + " (" + command_params.get("description", "Unknow") + ")")
    send('{"params":{"script":"' + code + '"},"id":123,"method":"gcode/script"}')


def send(message):
    print(">\t\tSEND:\t" + message)
    try:
        # sock.send(("%s\x03" % (message)).encode())  # Python 2
        sock.send(message.encode()+bytes([0x03]))   # Python 3
    except socket.error:
        connect()
        send(message)


def get_message():
    result = []
    try:
        # time.sleep()  # Debug mode
        print(">\t\t" + "RECV")
        data = sock.recv(4096)
        print(">\t\tDATA:\t" + str(data))
        for message in data.decode().split('\x03'):
            if message == '{"id":123,"result":{}}':
                continue
            if not message:
                continue
            print(">\t\tGET:\t" + message)
            try:
                output = str(json.loads(message).get("params", {}).get("response", ""))
                if not output:
                    output = str(json.loads(message).get("error", {}).get("message", ""))
                if not output:
                    continue
                result.append(output)
            except ValueError:
                continue
    except KeyboardInterrupt:
        sys.exit(-1)
    except socket.error:
        connect()
        return get_message()
    return result
